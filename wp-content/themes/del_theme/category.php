

<?php get_header() ?>
<section class="main-container">
  <div class="flex-container">
  <?php
        if ( is_category() ) { 
            $cat = get_category (get_query_var('cat')); 
        }
        $my_posts = null; 
        $my_posts = new WP_Query(array('post_type' => 'post', 'category_name' => $cat->slug));
  ?>
	  <?php if($my_posts->have_posts()) : ?>
	  	<?php while($my_posts->have_posts()) : $my_posts->the_post(); ?>		 
          <div class=" col">
            <div class="container-post">
                <div class="container-thumbnail">
                    <div class='post-background'></div>
                      <div class="center-image div-center">
                        <?php if (get_the_post_thumbnail_url()) {   ?>
                          <!-- Affiche le lien du post en cours  -->      
                            <?php if(get_post_thumbnail_id()){ ?>           
                              <div class="image">
                                <?php the_post_thumbnail('thumb', [
                                    'class' => 'myImage'
                                    ]) ?>
                                <?php } ?>                               
                              </div>
                    </div>
                <?php } ?>   
          
                <div class="container-text">
                  <div class="text-line"></div>
                  <span>view</span> 
                </div>     
          
              </div>
          </div>
    </div>
    <?php endwhile;
            wp_reset_query();
          ?>
    <?php else: ?>
            <p>Aucune photo a été trouvée.</p>
          <?php endif; ?>
  </div>
</section>
<?php get_footer() ?>



