<?php get_header(); ?>

<section class="main-container">
    <div class="flex-container">

        <div class='col-6'>
            <div class="container-contact">
                <?php
                while(have_posts()){
                the_post();
                    the_content();
                }    
                ?>
            </div>
        </div>
        <div class='col-6'>
           
        <?php the_post_thumbnail('medium', [
                                    'class' => 'my-picture'
                                    ]) ?>
                             
        </div>

    </div>
</section>
   
<?php get_footer() ?>

