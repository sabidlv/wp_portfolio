<?php



// enregistrer le main menu dans le dashboard:
function register_menu(){
    // pour déclarer l'emplacement du menu du theme(location, description)
    register_nav_menu('primary_menu', 'main-menu');   
}
add_action('init', 'register_menu');

add_theme_support('post-thumbnails');
add_image_size('pictures', 600, 500,true);

//ajout pour appliquer la feuille de style et le js
add_action('wp_enqueue_scripts', function(){
    // fct wp pui permet de register un style
    wp_register_style('myStyle', get_stylesheet_directory_uri().'/style.css');
    wp_enqueue_style('myStyle');

        //fct qui permet de register un script
        wp_register_script('myJs', 
            get_stylesheet_directory_uri().'/assets/js/main.js',
            [], false, true
        );
        wp_enqueue_script('myJs');
    
});







