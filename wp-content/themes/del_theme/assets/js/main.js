/** 
 * PAGE HOME
 */
function scrollAppear(){
        let screenPosition = window.innerHeight;
        let theFigures = document.querySelectorAll('.content-home img');
        let theText = document.querySelectorAll('.content-home p');
        theFigures.forEach(Element =>{
            let positionElement =  Element.getBoundingClientRect().top;
            if (positionElement < (screenPosition/1.8)){
            setTimeout(function(){Element.classList.add('content-appear--visible');},10);
            }
            else{
                Element.classList.remove('content-appear--visible');
            }
        });
        theText.forEach(Element=>{ 
            let positionElement =  Element.getBoundingClientRect().top;
            if (positionElement <= (screenPosition/1.5)){ 
            setTimeout(function(){
                Element.classList.add('contentp-appear--visible');},10);
            }else{
                Element.classList.remove('contentp-appear--visible');
            }
            });
        
    };

window.addEventListener('scroll', scrollAppear);



/*theFigures.forEach(Element =>{
    setTimeout(function(){Element.classList.add('content-appear--visible');},1000);
});
theText.forEach(Element=>{  
    setTimeout(function(){
        Element.classList.add('contentp-appear--visible');},1000);
    });
*/

/**
 * PAGE CATEGORIES: Urban and portrait
 */
let x = document.querySelectorAll('.container-post');
let y = document.querySelectorAll(' .container-post .post-background');
x.forEach(ele =>ele.addEventListener('click',()=>{
    ele.querySelector('.container-post .div-center').classList.toggle('center-image');
    ele.querySelector('.container-post .image').classList.toggle('click-image');
    ele.querySelector('.container-post .container-text').classList.toggle('container-text--color');
    for(let i = 0; i< y.length;i++){
        setTimeout(() => {
            y[i].classList.toggle('post-background--open');
        },10*i);
    } 
}));

/**
 * Gestion du menu responsive
 */
let navMenu = document.getElementById('nav-menu');
let linkMenu = document.querySelectorAll('.container-menu li');
navMenu.addEventListener('click',()=>{
    let contenu = navMenu.innerText;
    if (contenu === 'Menu'){
    navMenu.innerText= 'Close';
    document.getElementById('nav-div').style.visibility='visible';
   
    }else{
        navMenu.innerText= 'Menu';
        document.getElementById('nav-div').style.visibility='hidden';      
    }

});
    



/**
 * function docReady(fn) {
    // see if DOM is already available
    if(document.readyState === 'loading'){
        console.log("loading");
    
       
    }
    if (document.readyState === "complete" || document.readyState === "interactive") {
        // call on next available tick
        setTimeout(fn, 1);
    } else {
        document.addEventListener("DOMContentLoaded", fn);
        console.log('loader fini'); 
  
     
          }
       
        observer.observe(document.querySelectorAll('.content-appear')); 
    }
} 
docReady();

 */