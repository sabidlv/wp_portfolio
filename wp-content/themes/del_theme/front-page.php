<!--Home page, page statique diférente de la pageportfolio qui contiendra les post -->

<?php
/*Template Name: Home page*/
get_header();
?>


    <section class="main-container">
        <?php
        the_post();?>
        <div class="content-home">
        <?php the_content(); ?>
        </div>
    </section>

<?php get_footer() ?>


