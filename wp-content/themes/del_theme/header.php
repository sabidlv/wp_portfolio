<!DOCTYPE html>
<html <?php language_attributes() ?>>
<head>
    <meta charset="<?php bloginfo('charset') ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php bloginfo('name'); wp_title('-') ?></title>
    <?php wp_head() ?>
</head>
<body>
<div class='wrap'>
   
        <header>
   
            <nav>
            <span id="nav-menu">Menu</span>
                <?php 
                    // pour afficher le menu nav
                    wp_nav_menu([
                        'theme_location' => 'primary_menu',
                                //identifiant de l'emplacement déclarer dans function.php(register-nav-menu)
                        'menu_id' => 'container-menu', // id de la div qui contient ul ??
                        'menu_class' => 'container-menu', // class de la div de ul  ???
                        'container_class' => 'nav-div',
                        'container_id' => 'nav-div',
                    ]);
                ?>
            </nav>
        </header>  
    <main>   
