<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'portfolio' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '.VEF}`7V,4tGJkNbUCyrLE;%a|I$?tr%_6hHhsC:vSK%@xW))J/AOh;XOGQ= n(f' );
define( 'SECURE_AUTH_KEY',  '^PSc.tZn5=)mZ_0Scyyk,Vb0Fi&^2UUT]trxDKh$LOj9)Bkz{?*m<22~wN#hS&7s' );
define( 'LOGGED_IN_KEY',    '`)s{lE#8Iv+)(gz5q2}ZIDuKbZH%K`S]Yxk=COlF,.]5c4sn @NA;qTrbTuo[@nl' );
define( 'NONCE_KEY',        'g>aHRc7Z oB_mn$X=RV|YmxDWNNB5tJ`$_pOy-[ogBfa7a..004T)&ZwI`V$INyc' );
define( 'AUTH_SALT',        '7Y^rNOwF(Q)2;JO+!Y+RpS`sD0VNGex5 m6I;KjR7Z-V,gd~[4;@(=_:)lf&CpZ(' );
define( 'SECURE_AUTH_SALT', 'y}kYV6jpnetMPWyrp*3f#A{B^kr.@%:g8H-fe1#^NUDjPP9d(+6.n#0a) 8iF5U ' );
define( 'LOGGED_IN_SALT',   'PY_oj|v|67@++t7Sb@T5}bp ,!-1lw[x*:U/qXK|.ugR.Y=2B~|)FluzcuHk;Ilq' );
define( 'NONCE_SALT',       'zo4p]Tv>s$gE}hsh#Eo=$o8qIqt>Lsk3oj`1(Rq]V75d5bZnSo&1dxcZkqO><II ' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
